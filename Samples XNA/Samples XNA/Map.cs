﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Collision;
using FarseerPhysics.Common;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Controllers;
using System;
using System.Collections.Generic;

namespace FarseerPhysics.SamplesFramework
{
    public class Map
    {
        // définitions pour le moteur physique
        World world;
        MSTerrain _terrain;
        Body limites;
        AABB _container;

        // définitions pour le moteur graphique
        ScreenManager sc;
        Vector2 _origin;
        Effect _AlphaShader;
        int _scale;

        // Manipulations de textures
        Texture2D _waterTexture;
        Texture2D _CuttingDot;
        Texture2D _AlphaMask;
        Camera2D _camera;

        #region Initialize
        public Map(World w, ScreenManager sc, Camera2D cam, int scale)
        {
            _camera = cam;
            world = w;
            this.sc = sc;
            _scale = scale;

            _AlphaShader = sc.Content.Load<Effect>("Effects/AlphaTerrain");
            _CuttingDot = sc.Content.Load<Texture2D>("Effects/Circle");
            _waterTexture = sc.Content.Load<Texture2D>("Effects/eau");

        }

        private void SetWorldLimits(Texture2D texture) 
        {
            // limites du monde, 4 rectangles définissent un rectangle max
            List<Vertices> borders = new List<Vertices>(4);
            float borderWidth = 0.2f;
            float width = texture.Width/3;
            float height = texture.Height/2;

            //Bottom
            borders.Add(PolygonTools.CreateRectangle(width, borderWidth, new Vector2(0, height), 0));

            //Left
            borders.Add(PolygonTools.CreateRectangle(borderWidth, height, new Vector2(-width, 0), 0));

            //Top
            borders.Add(PolygonTools.CreateRectangle(width, borderWidth, new Vector2(0, -height), 0));

            //Right
            borders.Add(PolygonTools.CreateRectangle(borderWidth, height, new Vector2(width, 0), 0));

            Body body = BodyFactory.CreateCompoundPolygon(world, borders, 1, new Vector2(0, 0));
            body.BodyType = BodyType.Static;
            body.UserData = new UserData(EnumUserData.Limits); 

            limites = body;
            limites.OnCollision += new OnCollisionEventHandler(limites_OnCollision);

            //
            _container = new AABB(new Vector2(0, height - 15), texture.Width / 1.2f, 30);
            BuoyancyController buoyancy = new BuoyancyController(_container, 2, 2, 1, new Vector2(0, -8.2f));
            world.AddController(buoyancy);
        }

        bool limites_OnCollision(Fixture fixtureA, Fixture fixtureB, Dynamics.Contacts.Contact contact)
        {
            return true;
        }

#endregion

        #region Creation
        public void MSTerrainFromTexture(Texture2D texture)
        {
            Sprite sprite = new Sprite(texture);
            SetWorldLimits(texture);
            _origin = sprite.Origin;
            _AlphaMask = new Texture2D(sc.GraphicsDevice, texture.Width, texture.Height);

            _terrain = new MSTerrain(world, new AABB(Vector2.Zero, texture.Width/2, texture.Height/2))
            {
                PointsPerUnit = 2,
                CellSize = 20,
                SubCellSize = 5,
                Decomposer = Decomposer.Bayazit,
                Iterations = 2,
                Center = Vector2.Zero,
            };
            _terrain.Initialize();
            
            _terrain.ApplyTexture(texture, _terrain.Center, Color.Transparent);
           // SetWorldLimits();
        }

        #endregion

        #region Destroy part of the map

        #region MSTerrain manipulation
        private void DrawCircleOnMap(Vector2 center, sbyte value, float radius)
        {
            for (float by = -radius; by < radius; by += 0.1f)
            {
                for (float bx = -radius; bx < radius; bx += 0.1f)
                {
                    if ((bx * bx) + (by * by) < radius * radius)
                    {
                        float ax = bx + center.X;
                        float ay = by + center.Y;
                        _terrain.ModifyTerrain(new Vector2(ax, ay), value);
                    }
                }
            }
        }
        #endregion

        public void DestroyMSArea(float radius, Vector2 position)
        {
            DrawCircleOnMap(position, 1, radius);
            _terrain.RegenerateTerrain();

            Vector2 offset = new Vector2(
                             -_terrain.texture.Width / 2,
                             -_terrain.texture.Height / 2
                            );
            RemovePlanetChunk(position*2f - offset, radius/16);
        }

        private void RemovePlanetChunk(Vector2 RemovePosition, float radius)
        {
            RenderTarget2D target = new RenderTarget2D(sc.GraphicsDevice, _terrain.texture.Width, _terrain.texture.Height);
            // set the RenderTarget2D as the target for all future Draw calls untill we say otherwise
            sc.GraphicsDevice.SetRenderTarget(target);

            sc.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            sc.GraphicsDevice.Clear(Color.Transparent);

            // add in the previously drawn dots from the current alpha map.
            sc.SpriteBatch.Draw(_AlphaMask, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            // add a new dot to the map. 
            sc.SpriteBatch.Draw(_CuttingDot,
                    RemovePosition,
                    null,
                    Color.White,
                    0f,
                    new Vector2(_CuttingDot.Width / 2f, _CuttingDot.Height / 2f),
                    radius,
                    SpriteEffects.None,
                    0.9f);

            sc.SpriteBatch.End();

            // start drawing to the screen again
            sc.GraphicsDevice.SetRenderTarget(null);

            // set our Texture2D Alpha Mask to equal the current render target (the new mask).
            _AlphaMask = target;
        }
        #endregion

        #region Draw
        public void Draw(ScreenManager sc, Camera2D camera)
        {
            _AlphaShader.Parameters["MaskTexture"].SetValue(_AlphaMask);

            // start a spritebatch for our effect
            sc.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                null, null, null, _AlphaShader, camera.View);

            sc.SpriteBatch.Draw(_terrain.texture,
                Vector2.Zero,
                null, Color.White, 0f,
                _origin,
                (float)_scale, SpriteEffects.None, 0.9f);

            sc.SpriteBatch.End();

            // Draw water
            Rectangle target = new Rectangle();
            sc.SpriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap /* Must be set to Wrap */, null, null);
            sc.SpriteBatch.Draw(_waterTexture, _container.Center, target, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
            sc.SpriteBatch.End();        
        }
            #endregion
    }
}

