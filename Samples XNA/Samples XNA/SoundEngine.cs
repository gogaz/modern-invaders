﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;

namespace FarseerPhysics.SamplesFramework
{
    public enum SoundSample
    {
        explosion,
        menubip1,
        menubip2,
        mort,
        gun,
        hurt,
        marteau,
        grenade1

    }

    public enum SoundAmbiance
    {
        campagne1,
        future1
        
    }

    public class SoundEngine
    {
        Dictionary<SoundSample, SoundEffect> _soundEffects = new Dictionary<SoundSample, SoundEffect>(); 
        Dictionary<SoundAmbiance, Song> _soundAmbiance = new Dictionary<SoundAmbiance,Song>();
        bool isLooping;

        public SoundEngine(ContentManager cm)
        {
            _soundEffects[SoundSample.explosion] = cm.Load<SoundEffect>("Audio/explosion");
            _soundEffects[SoundSample.menubip1] = cm.Load<SoundEffect>("Audio/menubeep1");
            _soundEffects[SoundSample.menubip2] = cm.Load<SoundEffect>("Audio/menubeep2");
            _soundEffects[SoundSample.mort] = cm.Load<SoundEffect>("Audio/mort");
            _soundEffects[SoundSample.gun] = cm.Load<SoundEffect>("Audio/gun");
            _soundEffects[SoundSample.grenade1] = cm.Load<SoundEffect>("Audio/explosion");
            _soundEffects[SoundSample.hurt] = cm.Load<SoundEffect>("Audio/hurt");
            _soundAmbiance[SoundAmbiance.campagne1] = cm.Load<Song>("Audio/ambi-campagne-1");
            _soundAmbiance[SoundAmbiance.future1] = cm.Load<Song>("Audio/ambi-future-1");
            _soundEffects[SoundSample.marteau] = cm.Load<SoundEffect>("Audio/marteau");
        
            MediaPlayer.ActiveSongChanged += new EventHandler<EventArgs>(MediaPlayer_ActiveSongChanged);
           
        }

        void MediaPlayer_ActiveSongChanged(object sender, EventArgs e)
        {
            if (!isLooping)
                MediaPlayer.Stop();
        }

        public void playSample(SoundSample sound)
        {
            _soundEffects[sound].Play();
        }

        public void PlayMusic(SoundAmbiance sound, bool loop = true)
        {
            MediaPlayer.Stop();
            MediaPlayer.IsRepeating = loop;
            isLooping = loop;
            MediaPlayer.Play(_soundAmbiance[sound]);
        }

        public void StopMusic()
        {
            MediaPlayer.Stop();
            MediaPlayer.IsRepeating = false;
        }

        public bool Mute()
        {
           
           MediaPlayer.IsMuted = !MediaPlayer.IsMuted;
            if (MediaPlayer.IsMuted)
            {
                 return true;
            }
            else
                return false;

        }

    }

}
