﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;

namespace FarseerPhysics.SamplesFramework
{
    public enum WeapoonsAvailable
    {
        Gun,
        Bazooka,
        Marteau,
        Grenade1,
        //pompes
    }

    class WeapoonManager
    {
        World _world;
        ScreenManager _sc;

        Dictionary<WeapoonsAvailable, WeapoonItem> PlayerWeapoons;

        public WeapoonManager(World world, ScreenManager sc)
        {
            _world = world;
            _sc = sc;
            PlayerWeapoons = new Dictionary<WeapoonsAvailable, WeapoonItem>();
        }

        public void LoadContent()
        {
            PlayerWeapoons[WeapoonsAvailable.Bazooka] = new WeapoonItem(new Weapoons.Bazooka(_world, _sc));
            PlayerWeapoons[WeapoonsAvailable.Gun] = new WeapoonItem(new Weapoons.Gun(_world, _sc));
            PlayerWeapoons[WeapoonsAvailable.Marteau] = new WeapoonItem(new Weapoons.CoupdePoing(_world, _sc));
            PlayerWeapoons[WeapoonsAvailable.Grenade1] = new WeapoonItem(new Weapoons.Grenade1(_world, _sc));
            //PlayerWeapoons[WeapoonsAvailable.pompes] = new WeapoonItem(new Weapoons.Fusilpomp(_world, _sc));
        }
        

        public WeapoonItem Get(WeapoonsAvailable w)
        {
            return PlayerWeapoons[w];
        }

    }

    class WeapoonItem
    {
        public IWeapoon weapoon { get; set; }
        public int ammo { get; set; }
        public WeapoonItem NextItem { get; set; }

        public WeapoonItem()
        {
            weapoon = null;
            ammo = 0;
            NextItem = null;
        }

        public WeapoonItem(IWeapoon w)
        {
            ammo = -1;
            weapoon = w;
            NextItem = null;
        }

        public WeapoonItem(IWeapoon w, int ammo)
        {
            NextItem = null;
            weapoon = w;
            this.ammo = ammo;
        }
    }

    public interface IWeapoon
    {
        Body Body { get; }
        Body bullet { get; }
        bool CanShoot { get; }
        WeapoonsAvailable Type { get; }
        int Shots { get; set; }

        Body Shoot();
        void Draw(ScreenManager sc, GameTime gameTime, bool aimsens);
        void Update(GameTime gameTime);
        void Assign(Character c);
        void UnAssign();
    }
}
