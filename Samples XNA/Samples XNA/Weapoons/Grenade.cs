﻿//using System;
//using System.Collections.Generic;
//using FarseerPhysics.Common;
//using FarseerPhysics.Common.PolygonManipulation;
//using FarseerPhysics.Common.Decomposition;
//using FarseerPhysics.Collision.Shapes;
//using FarseerPhysics.Dynamics;
//using FarseerPhysics.Common.PhysicsLogic;
//using FarseerPhysics.Dynamics.Joints;
//using FarseerPhysics.Factories;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;

//namespace FarseerPhysics.SamplesFramework.Weapoons
//{
//    class Grenade : IWeapoon
//    {
//        public Body Body { get; internal set; }
//        public Body bullet { get; internal set; }
//        public float Time { get; internal set; }
//        public Map Map { get; set; }
//        public float radius = 8;
//        private World _world;
//        private Sprite _bulletSprite;
//        public bool CanShoot { get; internal set; }
//        private ScreenManager _sc;
//        private const float BulletForce = 10000f;
//        private float RotationAngle = 0f;
//        private Body _body;
//        private Character _player;
//        SimpleAnimationSprite explosion;

//        //public Grenade(World world, Map map, Vector2 position)
//        public Grenade(World world, ScreenManager sc)
//        {
//            _sc = sc;
//            CanShoot = true;
//            _world = world;
//            Body = BodyFactory.CreateCircle(world, radius / 4, 1f);
//            Body.AngularDamping = 0.3f;
//            //Body.Position = position;
//            Body.OnCollision += new OnCollisionEventHandler(Body_OnCollision);
//            Time = 5;
//            _bulletSprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/marteau"));
//            explosion = _sc.AnimManager.Get(AnimationSample.Bazooka_explosion);
//        }
//        public Body Shoot()
//        {
//            float angle = this.Body.Rotation;
//            if (CanShoot)
//            {
//                CanShoot = false;

//                // lance le son
//                _sc.SoundEngine.playSample(SoundSample.marteau);

//                Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

//                // Create a new bullet
//                bullet = new Body(_world);
//                Vertices vertices = PolygonTools.CreateEllipse(1.0f, 2f, 8);
//                PolygonShape polygon = new PolygonShape(vertices, 1f);
//                Fixture fixture = bullet.CreateFixture(polygon);

//                // Calculate the initial position of the bullet, at the end of the canon
//                bullet.Position = _body.Position + new Vector2((float)Math.Cos((double)_body.Rotation) * 5f,
//                                                                       (float)Math.Sin((double)_body.Rotation) * 2f);
//                bullet.Mass = 10f;
//                bullet.BodyType = BodyType.Dynamic;
//                // give the bullet the same rotation the weapoon
//                bullet.Rotation = _body.Rotation;
//                bullet.AngularDamping = 0;
//                bullet.UserData = new UserData(EnumUserData.Bullet, this._player);
//                bullet.OnCollision += new OnCollisionEventHandler(bullet_OnCollision);
//                Vector2 force = new Vector2(BulletForce * direction.X, BulletForce * direction.Y);
//                bullet.ApplyForce(ref force);
//                bullet.IsBullet = true;

//                return bullet;
//            }
//            else
//                return null;
//        }
//        bool Body_OnCollision(Fixture fixtureA, Fixture fixtureB, Dynamics.Contacts.Contact contact)
//        {
//            Map.DestroyMSArea(radius, Body.Position);
//            _world.RemoveBody(Body);
//            Body = null;
//            return true;
//        }

//        public void Update(GameTime gameTime)
//        {
//            Time -= (float)gameTime.ElapsedGameTime.TotalSeconds;
//            if (Time <= 0)
//            {
//                Map.DestroyMSArea(radius, Body.Position);
//                _world.RemoveBody(Body);
//                Body = null;
//            }
//            RotationAngle += Time;
//            float circle = MathHelper.Pi * 2;
//            RotationAngle = RotationAngle % circle;
//            if (bullet != null)
//            {
//                float dragConstant = 0.05f;
//                // Compute direction & speed
//                Vector2 bulletDirection = bullet.GetWorldVector(new Vector2(0.75f, 0));
//                Vector2 bulletFlightDirection = bullet.LinearVelocity;
//                if (bulletFlightDirection != Vector2.Zero)
//                    bulletFlightDirection.Normalize();
//                float bulletSpeed = bulletFlightDirection.Length();

//                float dot;
//                Vector2.Dot(ref bulletFlightDirection, ref bulletDirection, out dot);

//                float dragForceMagnitude = (1f - Math.Abs(dot)) * bulletSpeed * bulletSpeed * dragConstant * bullet.Mass;

//                Vector2 bulletTailPosition = bullet.GetWorldPoint(new Vector2(-1.4f, 0));
//                bullet.ApplyForce(dragForceMagnitude * (-bulletDirection), bulletTailPosition);
//                bullet.IsBullet = true;

//                float BulletAngle = bullet.Rotation;
//                UpdateBullet(BulletAngle);
//            }

//            this.explosion.Update(gameTime);
//        }

//        private void UpdateBullet(float angle)
//        {
//            Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
//            direction.Normalize();
//            Vector2 force = new Vector2(BulletForce * direction.X, BulletForce * direction.Y);
//            bullet.ApplyLinearImpulse(force);
//        }

//        bool bullet_OnCollision(Fixture fixtureA, Fixture fixtureB, Dynamics.Contacts.Contact contact)
//        {
//            Body body = fixtureA.Body;
//            if (fixtureB.Body == _player.Body || fixtureB.Body == _player.Head)
//                return false;

//            // supprime la roquette et autorise à tirer
//            _world.RemoveBody(body);
//            bullet = null;
//            CanShoot = true;

//            bool test;
//            try
//            {
//                test = ((UserData)fixtureB.Body.UserData).Type != EnumUserData.Limits;
//            }
//            catch (InvalidCastException)
//            {
//                test = fixtureB.Body.UserData is MSTerrain;
//            }


//            if (fixtureB.Body.UserData != null && test)
//            {
//                // lance le son
//                _sc.SoundEngine.playSample(SoundSample.explosion);

//                // détruis le décor & explose
//                _player.DestroyArea(10f, body.Position);
//                Explosion _explosion = new Explosion(_world);
//                _explosion.Activate(body.Position, 19, 50);


//                // explose
//                explosion.Position = ConvertUnits.ToDisplayUnits(body.Position);
//                explosion.Reset();

//                return true;
//            }
//            else
//                return false;
//        }

//        public void Assign(Character c)
//        {
//            UnAssign();
//            CreateWeappon(c);
//        }
//        private void CreateWeappon(Character Char)
//        {
//            _player = Char;
//            _body = BodyFactory.CreateRectangle(_world, 4.5f, 1.5f, 1f);
//            _body.BodyType = BodyType.Dynamic;
//            _body.Position = _player.Body.Position - new Vector2(0, 5f);
//            _body.CollisionCategories = Category.None;

//            RevoluteJoint armjoint = new RevoluteJoint(_body, Char.Body, _body.LocalCenter, Char.Body.LocalCenter + new Vector2(0f, -1f));
//            _world.AddJoint(armjoint);

//        }
//        public void UnAssign()
//        {
//            _world.RemoveBody(_body);
//            _player = null;
//        }

//        public void Draw(ScreenManager sc, GameTime gameTime, bool sens)
//        {
//            // on affiche l'arme
//            //if (_player != null)
//            //    sc.SpriteBatch.Draw(_sprite.Texture, ConvertUnits.ToDisplayUnits(_body.Position), null,
//            //           Color.White, _body.Rotation, _sprite.Origin, 0.55f, sens ? SpriteEffects.None : SpriteEffects.FlipVertically, 0f);

//            // on affiche la bullet si elle existe
//            if (!CanShoot && bullet != null)
//                sc.SpriteBatch.Draw(_bulletSprite.Texture, ConvertUnits.ToDisplayUnits(bullet.Position), null, Color.White,
//                                    bullet.Rotation, _bulletSprite.Origin, 2.5f, SpriteEffects.None, 0f);


//            if (!explosion.FinishedAnimation)
//                explosion.Draw(gameTime, 3);

//        }
//    }
//}


