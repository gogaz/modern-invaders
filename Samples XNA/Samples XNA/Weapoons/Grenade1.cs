﻿using System;
using System.Collections.Generic;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework.Weapoons
{
    class Grenade1 : IWeapoon
    {
        private Character _player;
        private World _world;
        private Body _body;
        private Sprite _sprite;
        private Sprite _bulletSprite;
        private ScreenManager _sc;

        public Body Body { get { return _body; } }
        public Body bullet { get; internal set; }
        public bool CanShoot { get; internal set; }
        private const float BulletForce = 10000f;
        SimpleAnimationSprite explosion; 
        public int Shots { get; set; }
        private const int shoots = 1;
        private double timer;
        public WeapoonsAvailable Type { get { return WeapoonsAvailable.Grenade1; } }


        public Grenade1(World world, ScreenManager sc)
        {
            CanShoot = true;

            _bulletSprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/grenade"));
            _sprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/grenade"));

            _world = world;
            _sc = sc;
            bullet = null;
            explosion = _sc.AnimManager.Get(AnimationSample.Bazooka_explosion);
            Shots = shoots;
            timer = 5;
        }

        public Body Shoot()
        {
            float angle = this.Body.Rotation;
            if (CanShoot)
            {
                // lance le son
                _sc.SoundEngine.playSample(SoundSample.marteau);

                Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

                // Create a new bullet
                bullet = new Body(_world);
                Vertices vertices = PolygonTools.CreateEllipse(1.0f, 2f, 8);
                PolygonShape polygon = new PolygonShape(vertices, 1f);
                Fixture fixture = bullet.CreateFixture(polygon);

                // Calculate the initial position of the bullet, at the end of the canon
                bullet.Position = _body.Position + new Vector2((float)Math.Cos((double)_body.Rotation) * 5f,
                                                                       (float)Math.Sin((double)_body.Rotation) * 2f);
                bullet.Mass = 10f;
                bullet.BodyType = BodyType.Dynamic;
                // give the bullet the same rotation the weapoon
                bullet.Rotation = _body.Rotation;
                bullet.AngularDamping = 0;
                bullet.UserData = new UserData(EnumUserData.Bullet, this._player);
                //bullet.OnCollision += new OnCollisionEventHandler(bullet_OnCollision);
                Vector2 force = new Vector2(BulletForce * direction.X, BulletForce * direction.Y);
                bullet.ApplyForce(ref force);
                bullet.AngularVelocity = MathHelper.TwoPi;
                bullet.IsBullet = true;

                return bullet;
            }
            else
                return null;
        }

        public void Update(GameTime gameTime)
        {
            this.explosion.Update(gameTime);

            if (bullet != null)
            {
                timer -= gameTime.ElapsedGameTime.TotalSeconds;
                if (timer <= 0)
                {
                    timer = 5;
                    bullet_explosion();
                }
            }
            else
            {
                if (Shots <= 0)
                    Shots = shoots;
            }
        }

        void bullet_explosion()
        {
            // supprime la roquette et autorise à tirer

            // lance le son
                _sc.SoundEngine.playSample(SoundSample.explosion);

                // détruis le décor & explose
                _player.DestroyArea(10f, bullet.Position);
                Explosion _explosion = new Explosion(_world);
                _explosion.Activate(bullet.Position, 19, 50);


                // explose
                explosion.Position = ConvertUnits.ToDisplayUnits(bullet.Position);
                explosion.Reset();
                _world.RemoveBody(bullet);
                bullet = null;
                Shots--;
        }

        public void Assign(Character c)
        {
            if (_body != null)
                UnAssign();

            _player = c;
            _body = BodyFactory.CreateRectangle(_world, 2f, 1f, 1f);
            _body.BodyType = BodyType.Dynamic;
            _body.Position = c.Body.Position - new Vector2(0, 5f);
            _body.CollisionCategories = Category.None;

            RevoluteJoint armjoint = new RevoluteJoint(_body, c.Body, _body.LocalCenter, c.Body.LocalCenter + new Vector2(0f, 0.8f));
            _world.AddJoint(armjoint);
        }

        public void UnAssign()
        {
            _world.RemoveBody(_body);
            _player = null;
        }

        public void Draw(ScreenManager sc, GameTime gameTime, bool sens)
        {
            // on affiche l'arme si elle est utilisée
            if (_player != null && bullet == null)
                sc.SpriteBatch.Draw(_sprite.Texture, ConvertUnits.ToDisplayUnits(_body.Position) + new Vector2(15, 0f), null,
                       Color.White, _body.Rotation, _sprite.Origin, 2.5f, sens ? SpriteEffects.None : SpriteEffects.FlipVertically, 0f);

            // on affiche la bullet si elle existe
            if (bullet != null)
            {
                sc.SpriteBatch.Draw(_bulletSprite.Texture, ConvertUnits.ToDisplayUnits(bullet.Position), null, Color.White,
                    bullet.Rotation, _bulletSprite.Origin, 2.5f, SpriteEffects.FlipHorizontally, 0f);
                //sc.SpriteBatch.DrawString(sc.Fonts.MenuSpriteFont, timer.ToString(), new Vector2(bullet.WorldCenter.X - 8, bullet.WorldCenter.Y - 3f * 64f), _player.Equipe.Color);
            }



            if (!explosion.FinishedAnimation)
                explosion.Draw(gameTime, 3);
        }
    }

}
