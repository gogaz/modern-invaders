﻿using System;
using System.Collections.Generic;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework.Weapoons
{
    class CoupdePoing : IWeapoon
    {
        public WeapoonsAvailable Type { get { return WeapoonsAvailable.Marteau; } }

        private Character _player;
        private World _world;
        private Body _body;
        private Sprite _sprite;
        private Sprite _bulletSprite;
        private ScreenManager _sc;

        public int Shots { get; set; }
        public Body Body { get { return _body; } }
        public Body bullet { get; internal set; }
        public bool CanShoot { get; internal set; }
        private const float BulletForce = 10000f;
        private float RotationAngle = 0f;
        private const int shoots = 1;


        public CoupdePoing(World world, ScreenManager sc)
        {
            CanShoot = true;

            _bulletSprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/marteau"));
            _sprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/marteau"));

            _world = world;
            _sc = sc;
            bullet = null;

            Shots = shoots;
            //explosion = _sc.AnimManager.Get(AnimationSample.Bazooka_explosion);

        }

        public Body Shoot()
        {
            float angle = this.Body.Rotation;
            if (CanShoot)
            {
                CanShoot = false;

                // lance le son
                _sc.SoundEngine.playSample(SoundSample.marteau);

                Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

                // Create a new bullet
                bullet = new Body(_world);
                Vertices vertices = PolygonTools.CreateEllipse(0.7f, 1f, 8);
                PolygonShape polygon = new PolygonShape(vertices, 1f);
                Fixture fixture = bullet.CreateFixture(polygon);

                // Calculate the initial position of the bullet, at the end of the canon
                bullet.Position = _body.Position + new Vector2((float)Math.Cos((double)_body.Rotation) * 5f,
                                                                       (float)Math.Sin((double)_body.Rotation) * 2f);
                bullet.Mass = 10f;
                bullet.BodyType = BodyType.Dynamic;
                // give the bullet the same rotation the weapoon
                bullet.Rotation = _body.Rotation;
                bullet.AngularDamping = 0;
                bullet.UserData = new UserData(EnumUserData.Bullet, this._player);
                bullet.OnCollision += new OnCollisionEventHandler(bullet_OnCollision);
                Vector2 force = new Vector2(BulletForce * direction.X, BulletForce * direction.Y);
                bullet.ApplyForce(ref force);
                bullet.IsBullet = true;
                bullet.ApplyTorque(8f);

                return bullet;
            }
            else
                return null;
        }

        public void Update(GameTime gameTime)
        {
            if (bullet == null)
                if (Shots <= 0)
                    Shots = shoots;
        }

        bool bullet_OnCollision(Fixture fixtureA, Fixture fixtureB, Dynamics.Contacts.Contact contact)
        {
            Body body = fixtureA.Body;
            if (fixtureB.Body == null)
            {
                return false;
            }
            if (fixtureB.Body == _player.Body || fixtureB.Body == _player.Head)
                return false;

            // supprime la roquette et autorise à tirer
            Shots--;
            bullet = null;
            _world.RemoveBody(body);
            CanShoot = true;

            bool test;
            try
            {
                test = ((UserData)fixtureB.Body.UserData).Type != EnumUserData.Limits;
            }
            catch (InvalidCastException)
            {
                test = fixtureB.Body.UserData is MSTerrain;
            }

            if (fixtureB.Body.UserData != null && test)
            {

                // détruis le décor & explose
                _player.DestroyArea(3f, body.Position);
                return true;
            }
            else
                return false;
        }

        public void Assign(Character c)
        {
            if (_body != null)
                UnAssign();

            _player = c;
            _body = BodyFactory.CreateRectangle(_world, 2f, 1f, 1f);
            _body.BodyType = BodyType.Dynamic;
            _body.Position = c.Body.Position - new Vector2(0, 5f);
            _body.CollisionCategories = Category.None;

            RevoluteJoint armjoint = new RevoluteJoint(_body, c.Body, _body.LocalCenter, c.Body.LocalCenter + new Vector2(0f, 0.8f));
            _world.AddJoint(armjoint);
        }

        public void UnAssign()
        {
            _world.RemoveBody(_body);
            _player = null;
        }

        public void Draw(ScreenManager sc, GameTime gameTime, bool sens)
        {
            // on affiche l'arme si elle est utilisée
            if (_player != null && bullet != null)
                sc.SpriteBatch.Draw(_sprite.Texture, ConvertUnits.ToDisplayUnits(_body.Position), null,
                       Color.White, _body.Rotation, _sprite.Origin, 0.55f, sens ? SpriteEffects.None : SpriteEffects.FlipVertically, 0f);

            // on affiche la bullet si elle existe
            if (bullet != null)
                sc.SpriteBatch.Draw(_bulletSprite.Texture, ConvertUnits.ToDisplayUnits(bullet.Position), null, Color.White,
                    RotationAngle, _bulletSprite.Origin, 2.5f, SpriteEffects.FlipHorizontally, 0f);

        }
    }

}
