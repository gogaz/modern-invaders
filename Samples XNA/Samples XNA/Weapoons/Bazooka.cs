﻿using System;
using System.Collections.Generic;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Common.PhysicsLogic;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework.Weapoons
{
    class Bazooka : IWeapoon
    {
        private Character _player;
        private World _world;
        private Body _body;
        private Sprite _sprite;
        private Sprite _bulletSprite;
        private ScreenManager _sc;

        public Body Body { get { return _body; } }
        public Body bullet { get; internal set; }
        public bool CanShoot { get; set; }
        public int Shots { get; set; }
        public WeapoonsAvailable Type { get { return WeapoonsAvailable.Bazooka; } }

        private const float BulletForce = 8f;
        private const int shoots = 1;
        SimpleAnimationSprite explosion;

        //public Bazooka(World world, Character Char, ScreenManager sc)
        public Bazooka(World world, ScreenManager sc)

        {
            CanShoot = true;

            _bulletSprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/bullet_bazooka"));
            
            _sprite = new Sprite(sc.Content.Load<Texture2D>("Weapoons/bazooka"));
            _world = world;
            //_player = Char;
            //CreateWeappon(Char);
            _sc = sc;
            explosion = _sc.AnimManager.Get(AnimationSample.Bazooka_explosion);
            Shots = shoots;
        }

        private void CreateWeappon(Character Char)
        {
            _player = Char;
            _body = BodyFactory.CreateRectangle(_world, 4.5f, 1.5f, 1f);
            _body.BodyType = BodyType.Dynamic;
            _body.Position = _player.Body.Position - new Vector2(0, 3f);
            _body.CollisionCategories = Category.None;

            RevoluteJoint armjoint = new RevoluteJoint(_body, Char.Body, _body.LocalCenter, Char.Body.LocalCenter + new Vector2(0f, -1f));
            _world.AddJoint(armjoint);

        }

        public Body Shoot()
        {
            float angle = this.Body.Rotation;
            if (CanShoot)
            {
                CanShoot = false;
                Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

                // Create a new bullet
                bullet = new Body(_world);
                Vertices vertices = PolygonTools.CreateRectangle(1.5f, 0.5f);
                PolygonShape polygon = new PolygonShape(vertices, 1f);
                Fixture fixture = bullet.CreateFixture(polygon);

                // Calculate the initial position of the bullet, at the end of the canon
                bullet.Position = _body.Position + new Vector2((float)Math.Cos((double)_body.Rotation) * 5f,
                                                                       (float)Math.Sin((double)_body.Rotation) * 2f);
                bullet.Mass = 10f;
                bullet.BodyType = BodyType.Dynamic;
                // give the bullet the same rotation the weapoon
                bullet.Rotation = _body.Rotation;
                bullet.AngularDamping = 3;
                bullet.UserData = new UserData(EnumUserData.Bullet, this._player);
                bullet.OnCollision += new OnCollisionEventHandler(bullet_OnCollision);

                return bullet;
            }
            else
                return null;
        }

        public void Update(GameTime gameTime)
        {
            if (bullet != null)
            {
                float dragConstant = 0.05f;
                // Compute direction & speed
                Vector2 bulletDirection = bullet.GetWorldVector(new Vector2(0.75f, 0));
                Vector2 bulletFlightDirection = bullet.LinearVelocity;
                if (bulletFlightDirection != Vector2.Zero)
                    bulletFlightDirection.Normalize();
                float bulletSpeed = bulletFlightDirection.Length();

                float dot;
                Vector2.Dot(ref bulletFlightDirection, ref bulletDirection, out dot);

                float dragForceMagnitude = (1f - Math.Abs(dot)) * bulletSpeed * bulletSpeed * dragConstant * bullet.Mass;

                Vector2 bulletTailPosition = bullet.GetWorldPoint(new Vector2(-1.4f, 0));
                bullet.ApplyForce(dragForceMagnitude * (-bulletDirection), bulletTailPosition);
                bullet.IsBullet = true;

                float BulletAngle = bullet.Rotation;
                UpdateBullet(BulletAngle);


            }
            else
            {
                if (Shots <= 0)
                    Shots = shoots;

            }

            this.explosion.Update(gameTime);

        }

        private void UpdateBullet(float angle)
        {
            Vector2 direction = new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            direction.Normalize();
            Vector2 force = new Vector2(BulletForce * direction.X, BulletForce * direction.Y);
            bullet.ApplyLinearImpulse(force);
        }

        bool bullet_OnCollision(Fixture fixtureA, Fixture fixtureB, Dynamics.Contacts.Contact contact)
        {
            Body body = fixtureA.Body;
            if (fixtureB.Body == _player.Body || fixtureB.Body == _player.Head || fixtureB.Body.IsBullet) 
                return false;

            Shots--;
            // supprime la roquette et autorise à tirer
            _world.RemoveBody(body);
            bullet = null;
            CanShoot = true;

            bool test;
            try
            {
                test = ((UserData)fixtureB.Body.UserData).Type != EnumUserData.Limits;
            }
            catch (InvalidCastException)
            {
                test = fixtureB.Body.UserData is MSTerrain;
            }


            if (fixtureB.Body.UserData != null && test )
            {
                // lance le son
                _sc.SoundEngine.playSample(SoundSample.explosion);

                // détruis le décor & explose
                _player.DestroyArea(10f, body.Position);
                Explosion _explosion = new Explosion(_world);
                _explosion.Activate(body.Position, 30, 50);


                // explose
                explosion.Position = ConvertUnits.ToDisplayUnits(body.Position);
                explosion.Reset();

                return true;
            }
            else
                return false;
        }

        public void Assign(Character c)
        {
            UnAssign();
            CreateWeappon(c);
        }

        public void UnAssign()
        {
            _world.RemoveBody(_body);
            _player = null;
        }

        public void Draw(ScreenManager sc, GameTime gameTime, bool sens)
        {
            // on affiche l'arme
            if (_player != null)
                sc.SpriteBatch.Draw(_sprite.Texture, ConvertUnits.ToDisplayUnits(_body.Position), null,
                       Color.White, _body.Rotation, _sprite.Origin, 0.55f, sens ? SpriteEffects.None : SpriteEffects.FlipVertically, 0f);

            // on affiche la bullet si elle existe
            if (!CanShoot && bullet != null)
                sc.SpriteBatch.Draw(_bulletSprite.Texture, ConvertUnits.ToDisplayUnits(bullet.Position), null, Color.White,
                                    bullet.Rotation, _bulletSprite.Origin, 2.5f, SpriteEffects.None, 0f);


            if (!explosion.FinishedAnimation)
                explosion.Draw(gameTime, 3);

            }
    }

}
