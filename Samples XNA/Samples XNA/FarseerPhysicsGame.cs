using System;
using System.Text;
using FarseerPhysics.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class FarseerPhysicsGame : Game
    {
        private GraphicsDeviceManager _graphics;

        public FarseerPhysicsGame()
        {
            Window.Title = "Modern Invaders";
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferMultiSampling = true;
#if WINDOWS || XBOX
            _graphics.PreferredBackBufferWidth = 1280;
            _graphics.PreferredBackBufferHeight = 720;
            ConvertUnits.SetDisplayUnitToSimUnitRatio(24f);
            IsFixedTimeStep = true;
#elif WINDOWS_PHONE
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 480;
            ConvertUnits.SetDisplayUnitToSimUnitRatio(16f);
            IsFixedTimeStep = false;
#endif
#if WINDOWS
            _graphics.IsFullScreen = false;
#elif XBOX || WINDOWS_PHONE
            _graphics.IsFullScreen = true;
#endif

            Content.RootDirectory = "Content";

            //new-up components and add to Game.Components
            GameSettings settings = new GameSettings();
            ScreenManager = new ScreenManager(this, settings);
            Components.Add(ScreenManager);

            //FrameRateCounter frameRateCounter = new FrameRateCounter(ScreenManager);
            //frameRateCounter.DrawOrder = 101;
            //Components.Add(frameRateCounter);

            // Charger le moteur son
            ScreenManager.SoundEngine = new SoundEngine(Content);

        }

        public ScreenManager ScreenManager { get; set; }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        /// 

        protected override void Initialize()
        {
            base.Initialize();


            Bateaux Bateaux = new Bateaux();
            Map1 Map1 = new Map1();
            Map2 Map2 = new Map2();
            DragonViolet DragonViolet = new DragonViolet();
            Fogsroad Fogsroad = new Fogsroad();
            ModernInvaders MI = new ModernInvaders();
            BossHouse BossHouse = new BossHouse();
            Ninja ninja = new Ninja();



            ScreenManager.AddScreen(new LogoScreen(TimeSpan.FromSeconds(5.0)));

            GameScreen menuBackground = new PhysicsMenuBackgroundScreen();
            ScreenManager.AddScreen(new BackgroundScreen());
            ScreenManager.AddScreen(menuBackground);

            ScreenManager.SoundEngine.PlayMusic(SoundAmbiance.future1);
            StartMenuScreen startmenu = new StartMenuScreen();
            startmenu.isMaster = true;

            MenuScreen menuScreen = new MenuScreen("Modern Invaders");
            MenuScreen menuUnjoueur = new MenuScreen("Un Joueur");
            MenuScreen menuMultijoueur = new MenuScreen("Multijoueur");
            MenuScreen menuOptions = new MenuScreen("Options");
            MenuScreen menuSon = new MenuScreen("Son");
            MenuScreen SoundOptions = new MenuScreen("Son");

            //startmenu.AddMenuItem(ScreenManager.Content.Load<Texture2D>("logo"), Vector2.Zero, menuScreen, ScreenManager);
            startmenu.AddMenuItem(ScreenManager.Content.Load<Texture2D>("Menuu/boutonjouer"), new Vector2(650, 100), menuUnjoueur, ScreenManager, 2f);
            startmenu.AddMenuItem(ScreenManager.Content.Load<Texture2D>("Menuu/boutonmulti"), new Vector2(650, 220), menuMultijoueur, ScreenManager, 2f);
            startmenu.AddMenuItem(ScreenManager.Content.Load<Texture2D>("Menuu/bouton options"), new Vector2(650, 340), menuOptions, ScreenManager, 2f);

            menuScreen.AddMenuItem("Un Joueur", EntryType.Screen, menuUnjoueur);
            menuUnjoueur.AddMenuItem("Campagne", EntryType.Screen, null);
            menuUnjoueur.AddMenuItem("Debuter la campagne", EntryType.Screen, null);
            menuUnjoueur.AddMenuItem("Acces direct a un niveau", EntryType.Screen, null);
            menuUnjoueur.AddMenuItem("", EntryType.Separator, null);
            menuUnjoueur.AddMenuItem("Retour", EntryType.ExitItem, null);

            menuScreen.AddMenuItem("Multijoueur", EntryType.Screen, menuMultijoueur);
            menuMultijoueur.AddMenuItem("Bateaux", EntryType.Screen, Bateaux.Reset());
            menuMultijoueur.AddMenuItem("Ninja", EntryType.Screen, ninja.Reset());
            menuMultijoueur.AddMenuItem("Modern Invaders", EntryType.Screen, MI.Reset());
            menuMultijoueur.AddMenuItem("Dragon Violet", EntryType.Screen, DragonViolet.Reset());
            menuMultijoueur.AddMenuItem("Fogsroad", EntryType.Screen, Fogsroad.Reset());
            menuMultijoueur.AddMenuItem("Boss House", EntryType.Screen, BossHouse.Reset());
            menuMultijoueur.AddMenuItem("Soutenance 2", EntryType.Screen, Map2.Reset());
            menuMultijoueur.AddMenuItem("Soutenance 1", EntryType.Screen, Map1.Reset());

            menuMultijoueur.AddMenuItem("", EntryType.Separator, null);
            menuMultijoueur.AddMenuItem("Retour", EntryType.ExitItem, null);

            menuScreen.AddMenuItem("Options", EntryType.Screen, menuOptions);
            menuOptions.AddMenuItem("Souris", EntryType.Screen, null);
            menuOptions.AddMenuItem("Clavier", EntryType.Screen, null);
            menuOptions.AddMenuItem("Son", EntryType.Screen, menuSon);
            menuOptions.AddMenuItem("Gestion des equipes", EntryType.Screen, null);
            menuOptions.AddMenuItem("Choix du personnage", EntryType.Screen, null);
            menuOptions.AddMenuItem("", EntryType.Separator, null);

            menuOptions.AddMenuItem("Retour", EntryType.ExitItem, null);

            // menuSon.AddMenuItem("MusiqueFuturiste1", EntryType.ExitItem, new SetSound().Reset());
            //  menuSon.AddMenuItem("MusiqueFuturiste2", EntryType.ExitItem, new SetSound());

            menuSon.AddMenuItem("", EntryType.Separator, null);
            menuSon.AddMenuItem("Retour", EntryType.ExitItem, null);

            menuScreen.AddMenuItem("", EntryType.Separator, null);
            menuScreen.AddMenuItem("Exit", EntryType.ExitItem, null);

            ScreenManager.AddScreen(startmenu);
            //ScreenManager.AddScreen(nouveaumonde);
        }
    }
}