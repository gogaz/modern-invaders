﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarseerPhysics.SamplesFramework
{
    class UserData
    {
        public EnumUserData Type { get; internal set; }
        public Character Character { get; set; }

        public UserData(EnumUserData eud)
        {
            Type = eud;
        }

        public UserData(EnumUserData eud, Character Character)
        {
            this.Character = Character;
        }
    }

    public enum EnumUserData
    {
        Wheel,
        Head,
        Body,
        Bullet,
        Limits,
        Other
        //Map // de type MSTerrain
    }

}
