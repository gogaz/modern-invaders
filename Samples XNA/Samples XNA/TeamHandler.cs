﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarseerPhysics.SamplesFramework
{
    class TeamHandler
    {
        /// <summary>
        /// Initialiser la liste de Teams donnée en paramètres
        /// </summary>
        /// <param name="teams">Teams list</param>
        private static void Initialize(ref List<Team> teams)
        {
            if (teams.Count == 0)
                throw new Exception("heu, non");

            for (int i = 0; i < teams.Count; i++)
            {
                teams[i].isPlaying = false;
            }

            teams[0].isPlaying = true;
        }
        /// <summary>
        /// Renvoie le personnage à utiliser
        /// </summary>
        /// <param name="teams"></param>
        /// <returns></returns>
        public static Character GetCurrentCharacter(List<Team> teams)
        {

            for (int i = 0; i < teams.Count; i++)
            {
                if (teams[i].isPlaying)
                {
                    return teams[i].GetActiveChar();
                }

            }
            Initialize(ref teams);
            SetNextCharacter(ref teams);
            return GetCurrentCharacter(teams);
        }

        /// <summary>
        /// Définis le personnage à utiliser
        /// </summary>
        /// <param name="teams"></param>
        public static void SetNextCharacter(ref List<Team> teams)
        {
            int i = 0;
            while (i < teams.Count)
            {
                if (teams[i].isPlaying)
                {
                    teams[i].isPlaying = false;
                    teams[i].NextActiveChar();

                    if (i + 1 >= teams.Count)
                    {
                        teams[0].isPlaying = true;
                    }
                    else
                    {
                        teams[i + 1].isPlaying = true;
                    }
                    break;
                }
                i++;
            }

            if (i >= teams.Count)
            {
                Initialize(ref teams);
            }
        }
    }

}
