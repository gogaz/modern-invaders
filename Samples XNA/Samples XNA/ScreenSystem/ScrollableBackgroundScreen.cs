﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    class ScrollableBackgroundScreen : GameScreen
    {
        private SpriteBatch _spriteBatch;

        // Background texture
        private ScreenManager _sc;

        // First cloud layer
        Texture2D _layer1;
        Vector2 _scroll1;

        // Second cloud layer
        Texture2D _layer2;
        Vector2 _scroll2;

        private Rectangle _viewport;

        public ScrollableBackgroundScreen(ScreenManager sc)
        {
            _sc = sc;
            Viewport viewport = sc.GraphicsDevice.Viewport;
            float border = viewport.Height;
            _viewport = viewport.Bounds;
            _scroll1 = Vector2.Zero;
            _scroll2 = Vector2.Zero;
        }

        public override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(_sc.GraphicsDevice);
            _layer1 = _sc.Content.Load<Texture2D>("Background/layer1");
            _layer2 = _sc.Content.Load<Texture2D>("Background/layer2");
            _sc.GraphicsDevice.Clear(Color.CornflowerBlue);
        }

        public override void Update(GameTime gameTime, bool otherScreenhasFocus, bool coveredbyOtherScreen)
        {
            // Scroll layer 1
            if (_scroll1.X > _layer1.Width)
                _scroll1.X = 0;
            if (_scroll2.X > _layer1.Width)
                _scroll2.X = 0;

            _scroll1.X += 0.5f;
            //_scroll1.Y += 0.5f;
            
            // Scroll layer 2
            _scroll2.X += 1.0f;
            //_scroll2.Y += 0.8f;

            base.Update(gameTime, otherScreenhasFocus, false);
        }

        public override void Draw(GameTime gameTime)
        {

            _spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap /* Must be set to Wrap */, null, null);
            // Draw the background
            //_spriteBatch.Draw(_bg, Vector2.Zero, Color.White);
            // Draw the scrolling layers
            _spriteBatch.Draw(_layer1, _viewport, new Rectangle((int)(-_scroll1.X), (int)(-_scroll1.Y), _layer1.Width, _layer1.Height), Color.White);
            _spriteBatch.Draw(_layer2, _viewport, new Rectangle((int)(-_scroll2.X), (int)(-_scroll2.Y), _layer2.Width, _layer2.Height), Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }

    }


}