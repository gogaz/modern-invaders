﻿using System.Collections.Generic;
using System.Text;
using System;

using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework
{
    /// <summary>
    /// The background screen sits behind all the other menu screens.
    /// It draws a background image that remains fixed in place regardless
    /// of whatever transitions the screens on top of it may be doing.
    /// </summary>
    public class PhysicsMenuBackgroundScreen : PhysicsGameScreen
    {
        private Texture2D _backgroundTexture;
        private Rectangle _viewport;
        Map _map;
        //List<Weapoons.Grenade1> grenades = new List<Weapoons.Grenade1>();

        /// <summary>
        /// Constructor.
        /// </summary>
        public PhysicsMenuBackgroundScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            _backgroundTexture = ScreenManager.Content.Load<Texture2D>("Maps/ModernInvaders");

            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            float border = viewport.Height;
            _viewport = viewport.Bounds;

            World.Gravity = new Vector2(0, 10);
            _map = new Map(World, ScreenManager, Camera, 1);
            _map.MSTerrainFromTexture(_backgroundTexture);
        }

        /// <summary>
        /// Updates the background screen. Unlike most screens, this should not
        /// transition off even if it has been covered by another screen: it is
        /// supposed to be covered, after all! This overload forces the
        /// coveredByOtherScreen parameter to false in order to stop the base
        /// Update method wanting to transition off.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                    bool coveredByOtherScreen)
        {
            base.Update(gameTime, false, false);
            //foreach (Weapoons.Grenade1 g in grenades)
            //{
            //    g.Update(gameTime);
            //    if (g.Body == null || g.Body.Position.Y > 1000)
            //        grenades.Remove(g);
            //}
        }

        public override void HandleInput(InputHelper input, GameTime gameTime)
        {
            base.Update(gameTime, false, false);
            //if (input.IsNewMouseButtonPress(MouseButtons.LeftButton))
            //{
            //    grenades.Add(new Weapoons.Grenade(World, _map, input.Cursor));
            //}
        }

        /// <summary>
        /// Draws the background screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            _map.Draw(ScreenManager, Camera);
            //ScreenManager.SpriteBatch.Begin();
            //ScreenManager.SpriteBatch.Draw(_backgroundTexture, _viewport, Color.White);
            //ScreenManager.SpriteBatch.End();
        }
    }
}