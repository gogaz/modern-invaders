﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace FarseerPhysics.SamplesFramework
{
    public enum AnimationSample
    {
        marche,
        Bazooka_explosion
    }
    public class AnimationManager
    {
        private Dictionary<AnimationSample, SimpleAnimationSprite> sprites = new Dictionary<AnimationSample, SimpleAnimationSprite>();
        private ScreenManager ScreenManager;

        public AnimationManager(ScreenManager sc)
        {
            ScreenManager = sc;
            sprites[AnimationSample.Bazooka_explosion] = Load("Effects/Bazooka", false, new Vector2(3, 4));
            sprites[AnimationSample.marche] = new SimpleAnimationSprite(ScreenManager, new SimpleAnimationDefinition()
                                                                        {
                                                                            AssetName = "Effects/marche",
                                                                            FrameRate = 10,
                                                                            FrameSize = new Vector2(100f, 107f),
                                                                            Loop = true,
                                                                            NbFrames = new Vector2(12, 1)
                                                                        });
            sprites[AnimationSample.marche].Initialize();
            sprites[AnimationSample.marche].LoadContent(ScreenManager.SpriteBatch);

        }

        public SimpleAnimationSprite Get(AnimationSample anim)
        {
            return sprites[anim];
        }
        public SimpleAnimationSprite Load(string assetname, int framerate, Vector2 framesize, bool loop, Vector2 nbframes)
        {
            SimpleAnimationSprite NewAnimationSprite = new SimpleAnimationSprite(ScreenManager, new SimpleAnimationDefinition()
            {
                AssetName = assetname,
                FrameRate = framerate,
                FrameSize = framesize,
                Loop = loop,
                NbFrames = nbframes
            });
            NewAnimationSprite.Initialize();
            NewAnimationSprite.LoadContent(ScreenManager.SpriteBatch);
            return NewAnimationSprite;
        }
        private SimpleAnimationSprite Load(string path, bool loop, Vector2 nbFrames)
        {
            SimpleAnimationSprite AnimSprite;
            AnimSprite = new SimpleAnimationSprite(ScreenManager, new SimpleAnimationDefinition()
            {
                AssetName = path,
                FrameRate = 20,
                FrameSize = new Vector2(256f, 128f),
                Loop = loop,
                NbFrames = nbFrames
            });
            AnimSprite.Initialize();
            AnimSprite.LoadContent(ScreenManager.SpriteBatch);
            return AnimSprite;
        }
    }
}
