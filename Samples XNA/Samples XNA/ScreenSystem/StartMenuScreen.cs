using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    /// <summary>
    /// Base class for screens that contain a menu of options. The user can
    /// move up and down to select an entry, or cancel to back out of the screen.
    /// </summary>
    public class StartMenuScreen : GameScreen
    {
#if WINDOWS || XBOX
        private const float NumEntries = 15;
#elif WINDOWS_PHONE
        private const float NumEntries = 9;
#endif
        private List<MenuButton> _menuEntries = new List<MenuButton>();
        private MenuButton Mute;
        private MenuButton Quit;
        private Sprite _muteSprite;
        private Sprite _unmuteSprite;
        private Sprite _quitSprite;

        /// <summary>
        /// Constructor.
        /// </summary>
        public StartMenuScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.7);
            TransitionOffTime = TimeSpan.FromSeconds(0.7);
            HasCursor = true;
        }

        public void AddMenuItem(Texture2D texture, Vector2 position, GameScreen screen, ScreenManager sc)
        {
            MenuButton button = new MenuButton(texture, false, position, screen, sc);
            _menuEntries.Add(button);
        }

        public void AddMenuItem(Texture2D texture, Vector2 position, GameScreen screen, ScreenManager sc, float scale)
        {
            MenuButton button = new MenuButton(texture, false, position, screen, sc, scale);
            _menuEntries.Add(button);
        }


        public override void LoadContent()
        {
            base.LoadContent();

            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            SpriteFont font = ScreenManager.Fonts.MenuSpriteFont;

            _muteSprite = new Sprite(ScreenManager.Content.Load<Texture2D>("Menuu/UNMUTE"));
            _unmuteSprite = new Sprite(ScreenManager.Content.Load<Texture2D>("Menuu/MUTE"));
            _quitSprite = new Sprite(ScreenManager.Content.Load<Texture2D>("Menuu/boutonquit"));

            Mute = new MenuButton(_muteSprite.Texture, false, new Vector2(650, 600), this, ScreenManager, 0.5f);
            Quit = new MenuButton(_quitSprite.Texture, false, new Vector2(650, 460), null, ScreenManager, 1f);

        }

        /// <summary>
        /// Responds to user input, changing the selected entry and accepting
        /// or cancelling the menu.
        /// </summary>
        public override void HandleInput(InputHelper input, GameTime gameTime)
        {
            if (input.IsCursorValid)
            {
                Mute.Collide(input.Cursor);
                Quit.Collide(input.Cursor);
            }
            else
            {
                Mute.Hover = false;
                Quit.Hover = false;
            }

            if (input.IsMenuPressed())
            {
                if (Mute.Hover)
                {
                    if (ScreenManager.SoundEngine.Mute())
                        Mute.Texture = _unmuteSprite.Texture;
                    else
                        Mute.Texture = _muteSprite.Texture;
                }
                if (Quit.Hover)
                {
                    ScreenManager.SoundEngine.playSample(SoundSample.menubip2);
                    ScreenManager.Game.Exit();
                }
            }

            // Mouse or touch on a menu item
            foreach (MenuButton button in _menuEntries)
            {
                if (input.IsCursorValid)
                {

                    button.Collide(input.Cursor);
                }
                else
                {
                    button.Hover = false;
                }
            }

            foreach (MenuButton button in _menuEntries)
            {
                if (input.IsMenuPressed())
                {
                    if (button.Hover)
                    {
                        ScreenManager.SoundEngine.playSample(SoundSample.menubip1);
                        ScreenManager.AddScreen(button.Screen.Reset());
                    }
                }
            }



        }

        /// <summary>
        /// Allows the screen the chance to position the menu entries. By default
        /// all menu entries are lined up in a vertical list, centered on the screen.
        /// </summary>

        /// <summary>
        /// Updates the menu.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                    bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
            Quit.Update(gameTime);
            Mute.Update(gameTime);
            // Update each nested MenuEntry object.
            for (int i = 0; i < _menuEntries.Count; ++i)
            {
                _menuEntries[i].Update(gameTime);
            }
        }

        /// <summary>
        /// Draws the menu.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Fonts.MenuSpriteFont;

            spriteBatch.Begin();
            //Draw each menu entry in turn
            for (int i = 0; i < _menuEntries.Count; ++i)
            {
                _menuEntries[i].Draw();

            }

            Mute.Draw();
            Quit.Draw();
            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            Vector2 transitionOffset = new Vector2(0f, (float)Math.Pow(TransitionPosition, 2) * 100f);

            spriteBatch.End();
        }
    }
}