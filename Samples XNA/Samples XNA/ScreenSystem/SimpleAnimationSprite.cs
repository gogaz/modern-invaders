﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace FarseerPhysics.SamplesFramework
{
    public class SimpleAnimationSprite
    {

        public Vector2 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value -this.Definition.FrameSize/2f;
            }
        }
        private Vector2 _position;
        protected ScreenManager sc;
        protected SimpleAnimationDefinition Definition;
        public bool isLooping { get { return Definition.Loop; } set { Definition.Loop = value; } }
        protected SpriteBatch spriteBatch;
        protected Sprite sprite;
        protected Vector2 CurrentFrame;
        public bool FinishedAnimation = true;

        #region Framerate Property
        private int _Framerate = 60;
        protected double TimeBetweenFrame = 16; // 60 fps
        protected double lastFrameUpdatedTime = 0;
        public int Framerate
        {
            get { return this._Framerate; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("Framerate can't be less or equal to 0");
                if (this._Framerate != value)
                {
                    this._Framerate = value;
                    this.TimeBetweenFrame = 1000.0d / (double)this._Framerate;
                }
            }
        }
        #endregion

        public SimpleAnimationSprite(ScreenManager sc, SimpleAnimationDefinition definition)
        {
            this.sc = sc;
            this.Definition = definition;
            _position = new Vector2();
            this.CurrentFrame = new Vector2();
        }

        public void Initialize()
        {
            this.Framerate = this.Definition.FrameRate;
        }

        public void Stop()
        {
            this.FinishedAnimation = true;
            Definition.Loop = false;
        }

        public void LoadContent(SpriteBatch spritebatch)
        {
            this.sprite = new Sprite(this.sc.Content.Load<Texture2D>(this.Definition.AssetName));
            if (spritebatch == null)
                this.spriteBatch = sc.SpriteBatch;
            else
                this.spriteBatch = spritebatch;
        }

        public void Reset(bool loop = false)
        {
            if (!FinishedAnimation)
                return;

            Definition.Loop = loop;
            this.CurrentFrame = new Vector2();
            this.FinishedAnimation = false;
            this.lastFrameUpdatedTime = 0;
        }

        public void Update(GameTime time)
        {
            if (FinishedAnimation)
            {
                if (isLooping)
                    FinishedAnimation = false;
                else
                    return;
            }
            this.lastFrameUpdatedTime += time.ElapsedGameTime.Milliseconds;
            if (this.lastFrameUpdatedTime > this.TimeBetweenFrame)
            {
                this.lastFrameUpdatedTime = 0;
                if (this.Definition.Loop)
                {
                    this.CurrentFrame.X++;
                    if (this.CurrentFrame.X >= this.Definition.NbFrames.X)
                    {
                        this.CurrentFrame.X = 0;
                        this.CurrentFrame.Y++;
                        if (this.CurrentFrame.Y >= this.Definition.NbFrames.Y)
                        {
                            this.CurrentFrame.Y = 0;
                            this.FinishedAnimation = true;
                        }
                    }
                }
                else
                {
                    this.CurrentFrame.X++;
                    if (this.CurrentFrame.X >= this.Definition.NbFrames.X)
                    {
                        this.CurrentFrame.X = 0;
                        this.CurrentFrame.Y++;
                        if (this.CurrentFrame.Y >= this.Definition.NbFrames.Y)
                        {
                            this.CurrentFrame.X = this.Definition.NbFrames.X - 1;
                            this.CurrentFrame.Y = this.Definition.NbFrames.Y - 1;
                            this.FinishedAnimation = true;
                        }
                    }
                }
            }
        }

        public void Draw(GameTime time, int scale)
        {
            this.spriteBatch.Draw(this.sprite.Texture,
                                    new Rectangle((int)_position.X, (int)_position.Y, (int)this.Definition.FrameSize.X * scale, (int)this.Definition.FrameSize.Y * scale),
                                    new Rectangle((int)this.CurrentFrame.X * (int)this.Definition.FrameSize.X, (int)this.CurrentFrame.Y * (int)this.Definition.FrameSize.Y, (int)this.Definition.FrameSize.X, (int)this.Definition.FrameSize.Y),
                                    Color.White, 0f, Definition.FrameSize/4, SpriteEffects.None, 1f);
        }

        public void Draw(GameTime time, int scale, SpriteEffects se)
        {
            this.spriteBatch.Draw(this.sprite.Texture,
                        new Rectangle((int)_position.X, (int)_position.Y, (int)this.Definition.FrameSize.X * scale, (int)this.Definition.FrameSize.Y * scale),
                        new Rectangle((int)this.CurrentFrame.X * (int)this.Definition.FrameSize.X, (int)this.CurrentFrame.Y * (int)this.Definition.FrameSize.Y, (int)this.Definition.FrameSize.X, (int)this.Definition.FrameSize.Y),
                        Color.White, 0f, Vector2.Zero, se, 1f);

        }
    }

    public class SimpleAnimationDefinition
    {
        public string AssetName { get; set; }
        public Vector2 FrameSize { get; set; }
        public Vector2 NbFrames { get; set; }
        public int FrameRate { get; set; }
        public bool Loop { get; set; }
    }

}
