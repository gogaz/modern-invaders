﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    public class Team
    {
        public List<Team> Teams { get; set; }
        public GameScreen thisGameScreen { get; set; }
        private List<Character> Characters;
        private WeapoonManager _wManager;
        private WeapoonsAvailable usedWeapoon;
        private List<WeapoonsAvailable> _available;

        public bool isPlaying = false;
        public bool isAlive { get; internal set; }
        public Color Color { get; set; }

        private int ActiveChar = 0;
        public string Name { get; internal set; }
        public Map levelMap;

        public Team(string nom, World world, ScreenManager sc)
        {
            Characters = new List<Character>();
            Name = nom;
            isAlive = true;
            usedWeapoon = WeapoonsAvailable.Gun;
            _wManager = new WeapoonManager(world, sc);
            _wManager.LoadContent();
            _available = (Enum.GetValues(typeof(WeapoonsAvailable)).Cast<WeapoonsAvailable>().ToList<WeapoonsAvailable>());

            Random rnd = new Random(nom.GetHashCode());
            Color = new Color(rnd.Next(0, 255), rnd.Next(0, 255), rnd.Next(0, 255));
        }

        public bool CheckAlives()
        {
            if (Characters.Count <= 0)
            {
                //thisGameScreen.Reset();
                isAlive = false;
                thisGameScreen.ExitScreen();
                return false;
            }
            else
                return true;
        }

        public void LoadCharacter(Character c)
        {
            c.Equipe = this;
            c.Map = levelMap;
            c.LoadContent();
            Characters.Add(c);
        }


        public void RemoveCharacter(Character c)
        {
            Characters.Remove(c);
            if (ActiveChar >= Characters.Count)
            {
                ActiveChar = 0;
            }
            this.CheckAlives();
        }

        private void HandleInput(InputHelper input, GameTime gameTime)
        {
            Characters[ActiveChar].Handle(input, gameTime);
        }

        public void NextActiveChar()
        {
            if (!this.CheckAlives())
                return;


            if (ActiveChar + 1 >= Characters.Count)
            {
                ActiveChar = 0;
            }
            else
            {
                ActiveChar += 1;
            }

            WeapoonItem weapoon = _wManager.Get(usedWeapoon);
            usedWeapoon = weapoon.weapoon.Type;
            if (!Characters[ActiveChar].SetWeapoon(weapoon.weapoon))
                return;
        }

        public void ChangeWeapoon(int value, Character c)
        {
            int position = _available.IndexOf(usedWeapoon);
            position += value;

            if (position >= _available.Count)
                position = 0;
            if (position < 0)
            {
                position = _available.Count - 1;
            }
            foreach (WeapoonsAvailable w in _available)
            {
                Console.WriteLine(w.ToString() + " " + _available.IndexOf(w));
            }

            if (!Characters[ActiveChar].SetWeapoon(_wManager.Get(_available[position]).weapoon))
                return;
            else
                usedWeapoon = _available[position];
        }

        public Character GetActiveChar()
        {
            this.CheckAlives();
            return Characters[ActiveChar];
        }

        public void Update(GameTime gameTime, Camera2D Camera)
        {
            if (!this.CheckAlives())
                return;
            for (int i = 0; i < Characters.Count; i++)
            {
                Characters[i].Update(gameTime);

                if (i < Characters.Count && Characters[i].hasPlayed)
                {
                    List<Team> t = Teams;
                    TeamHandler.SetNextCharacter(ref t);
                    Characters[i].hasPlayed = false;
                    Camera.TrackingBody = TeamHandler.GetCurrentCharacter(t).Body;
                    break;
                }
            }

        }
        public void DrawAll(ScreenManager sc, GameTime gameTime)
        {
            foreach (Character c in Characters)
            {
                c.Draw(sc, gameTime);
            }
        }

        #region Overriding System.Object
        public override string ToString()
        {
            return String.Format("", null);
        }
        #endregion

    }
}

