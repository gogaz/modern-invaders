﻿using System;
using System.Linq;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework
{
    public class Character
    {
        private const float _maxSpeed = 50.0f;

        public Map Map { get; set; }
        public Body Body { get { return _body; } }
        public Body Head { get { return _headBody; } }
        public bool DrawLife { get { return drawLife; } set { drawLife = value; } }
        public Team Equipe { get; set; }
        public string Name { get { return _name; } }
        public bool IsTouchingGround { get; internal set; }
        public bool hasPlayed { get; set; }

        public int Vie
        {
            get { return _vie; }
            set
            {
                if (_vie > value)
                    sc.SoundEngine.playSample(SoundSample.hurt);
                _vie = value;
            }
        }
        public int _vie = 100;
        private double DieIn = 0;
        private bool removeCorps = true;

        private World _world;
        private Body _body;
        private Body _headBody;
        private Body _wheel;
        private RevoluteJoint _wheeljoint;

        private Sprite _bodySprite;
        private Sprite _headSprite;
        private Color DamageColor { get { return new Color(Vie * 100 / 255, 255 - Vie * 100 / 255, 0); } }
        private bool drawName = true;
        private bool drawLife = true;
        private bool sens; // true droite, false gauche
        private bool aimsens; //    ""         ""     
        private bool WheelActivated;
        SimpleAnimationSprite marche;


        private Vector2 position;
        private string _name;
        private float collisionangle;

        public IWeapoon Weapoon
        {
            get { return _weapoon; }
            set
            {
                SetWeapoon(value);
            }
        }
        private IWeapoon _weapoon;



        private ScreenManager sc;

        #region Creation
        public Character(World world, Vector2 position, string nom, ScreenManager sc)
        {
            _world = world;
            this.sc = sc;
            this.position = position;
            _name = nom;
            hasPlayed = false;
        }

        public void LoadContent()
        {
            marche = sc.AnimManager.Load("Effects/marche", 10, new Vector2(100, 107), true, new Vector2(12, 1));
            _bodySprite = new Sprite(sc.Content.Load<Texture2D>("spritenormal"));

            //buste du personnage
            _body = BodyFactory.CreateRectangle(_world, 2f, 3.6f, 1f);
            _body.Position = position;
            _body.Friction = 0.7f;
            _body.Mass = 30f;
            _body.CollisionCategories = Category.Cat2;
            _body.UserData = new UserData(EnumUserData.Body, this);
            _body.BodyType = BodyType.Dynamic;

            // Tete du personnage
            _headSprite = new Sprite(sc.Content.Load<Texture2D>("spritehead"));
            _headBody = BodyFactory.CreateCircle(_world, 1f, 1f, 1f);
            _headBody.BodyType = BodyType.Dynamic;
            _headBody.Position = _body.WorldCenter;
            _headBody.Friction = 0f;
            _headBody.IgnoreCollisionWith(_body);
            _headBody.FixedRotation = true;
            _headBody.UserData = new UserData(EnumUserData.Head, this);

            RevoluteJoint headjoint = new RevoluteJoint(_headBody, _body, _headBody.LocalCenter, _body.LocalCenter + new Vector2(0f, -2f));
            headjoint.Enabled = true;
            _world.AddJoint(headjoint);

            // initilisation de la roue motrice du personnage
            _wheel = BodyFactory.CreateCircle(_world, 1f, 1f);
            _wheel.BodyType = BodyType.Dynamic;
            _wheel.Position = position;
            _wheel.Friction = 42f;
            _wheel.Restitution = 0f;
            _wheel.CollisionCategories = Category.Cat1;
            _wheel.UserData = new UserData(EnumUserData.Wheel, this);

            _headBody.OnCollision += new OnCollisionEventHandler(_headBody_OnCollision);
            _body.OnCollision += new OnCollisionEventHandler(_body_OnCollision);
            _wheel.OnCollision += new OnCollisionEventHandler(_wheel_OnCollision);
            _wheel.OnSeparation += new OnSeparationEventHandler(_wheel_OnSeparation);

            // accroche la roue au reste du corps
            _wheeljoint = new RevoluteJoint(_body, _wheel, new Vector2(0f, 2f), _wheel.LocalCenter);
            _world.AddJoint(_wheeljoint);

            ////////////////////
            // NE PAS TOUCHER //
            // Force le personnage à rester droit
            FixedAngleJoint resteDebout = new FixedAngleJoint(_body);
            resteDebout.TargetAngle = 0f;
            resteDebout.Enabled = true;
            _world.AddJoint(resteDebout);
            ////////////////////

            // Attribue une arme au personnage
            //_weapoon = new Weapoons.Bazooka(_world, this, sc);
        }

        #endregion
        #region Collisions
        void _wheel_OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            if (fixtureB.Body.UserData is MSTerrain)
            {
                IsTouchingGround = true;
                ContactEdge c = _wheel.ContactList;
                while (c != null && !IsTouchingGround)
                {
                    Body attached = c.Other;
                    if (attached.UserData is MSTerrain && attached != fixtureB.Body)
                    {
                        IsTouchingGround = true;
                    }
                    c = c.Next;
                }
            }
        }

        private bool _wheel_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {

            Vector2 position = contact.Manifold.LocalNormal;
            collisionangle = (float)Math.Atan2(position.Y, position.X);
            Vector2 force = Vector2.Zero; //stores the force of the impact

            if (collisionangle < 0) //detect the angle
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi + collisionangle) * fixtureA.Body.LinearVelocity.Y);
            else
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi - collisionangle) * fixtureA.Body.LinearVelocity.Y);

            //apply the collision angle to the velocity to work out the impact vel
            //now to get the force of the collison just do force.Length()
            //you now have the impact force on collison

            if (force.Length() > 20f)
                this.Vie -= (int)(force.Length() / 10f);

            if (fixtureB.Body.UserData is MSTerrain)
                IsTouchingGround = true;
            else
                if (((UserData)fixtureB.Body.UserData).Type == EnumUserData.Limits)
                    Vie = -42;

            return true;
        }

        public void DestroyArea(float radius, Vector2 position)
        {
            Map.DestroyMSArea(radius, position);
        }

        bool _body_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if (_weapoon != null && fixtureB.Body == _weapoon.Body)
                return false;

            Vector2 position = contact.Manifold.LocalNormal;
            float collisionangle = (float)Math.Atan2(position.Y, position.X);

            Vector2 force = Vector2.Zero; //stores the force of the impact

            if (collisionangle < 0) //detect the angle
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi + collisionangle) * fixtureA.Body.LinearVelocity.Y);
            else
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi - collisionangle) * fixtureA.Body.LinearVelocity.Y);

            this.Vie -= (int)(force.Length() / 10f);

            return true;
        }

        bool _headBody_OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if ((_weapoon != null && fixtureB.Body == _weapoon.Body) || (fixtureB.Body.UserData is UserData && ((UserData)fixtureB.Body.UserData).Character != this))
                return false;
            Vector2 position = contact.Manifold.LocalNormal;
            float collisionangle = (float)Math.Atan2(position.Y, position.X);

            Vector2 force = Vector2.Zero; //stores the force of the impact

            if (collisionangle < 0) //detect the angle
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi + collisionangle) * fixtureA.Body.LinearVelocity.Y);
            else
                force = new Vector2((float)(Math.Cos(collisionangle) * fixtureA.Body.LinearVelocity.X), (float)Math.Sin(MathHelper.TwoPi - collisionangle) * fixtureA.Body.LinearVelocity.Y);

            this.Vie -= (int)(force.Length() / 8f);

            return true;
        }
        #endregion

        public void Update(GameTime gameTime)
        {
            if (_weapoon != null)
            {
                if (_weapoon.Shots <= 0)
                {
                    hasPlayed = true;
                }
                _weapoon.Update(gameTime);
            }
            if (WheelActivated)
            {
                Walk(sens);
                if (marche.FinishedAnimation)
                    marche.Reset(true);
            }
            else
            {
                _wheel.FixedRotation = true;
                _wheel.AngularVelocity = 0f;
                marche.Stop();
            }

            marche.Position = ConvertUnits.ToDisplayUnits(_body.Position + new Vector2(0, 1));
            marche.Update(gameTime);

            if (Vie <= 0f)
            {
                if (DieIn <= 0)
                    Die(removeCorps);
                else
                    DieIn -= gameTime.ElapsedGameTime.TotalSeconds;
            }
        }

        public void Die(bool remove = true, double time = 0)
        {
            Vie = -42;
            removeCorps = remove;
            if (time > 0)
                return;
            else
                DieIn = time;

            //animations de mort
            sc.SoundEngine.playSample(SoundSample.mort);

            if (remove)
            {
                // remove character components
                _world.RemoveBody(_body);
                _world.RemoveBody(_wheel);
                _world.RemoveBody(_headBody);
                if (_weapoon != null)
                    _world.RemoveBody(_weapoon.Body);
            }
            //_body.Mass = int.MaxValue;

            // remove character from team list
            Equipe.RemoveCharacter(this);
            //Equipe.NextActiveChar();
        }
        #region Handle
        public void Handle(InputHelper input, GameTime gameTime)
        {
            if (input.IsKeyDown(Keys.D))
            {
                //this.WalkRight();
                sens = true;
                WheelActivated = true;
                _wheel.FixedRotation = false;
            }
            if (input.IsKeyDown(Keys.Q))
            {
                //this.WalkLeft();
                sens = false;
                WheelActivated = true;
                _wheel.FixedRotation = false;
            }
            if (input.IsNewKeyRelease(Keys.D) || input.IsNewKeyRelease(Keys.Q))
                WheelActivated = false;

            if (input.IsNewKeyPress(Keys.Space) && IsTouchingGround)
            {
                this.Jump(sens);
            }
            if ((input.IsKeyUp(Keys.D) || input.IsKeyUp(Keys.Q)) && IsTouchingGround)
            {
            }
        }
        public bool HandleMouse(InputHelper input, Camera2D Camera)
        {
            int changew = input.WheelValueChanged;

            if (changew != 0)
            {
                Equipe.ChangeWeapoon(changew, this);
            }

            Vector2 position = Camera.ConvertScreenToWorld(input.Cursor);

            if (Weapoon == null)
                return false;

            if (input.IsNewMouseButtonPress(MouseButtons.LeftButton))
            {
                Body b = _weapoon.Shoot();
                if (b != null)
                    Camera.TrackingBody = b;
                else
                    Camera.TrackingBody = _body;
                //Camera.Zoom = 1 / 3f;
                //OnShoot(this, _weapoon.Shoot(_weapoon.Body.Rotation));
                if (Weapoon.Shots <= 0)
                    return true;
                else
                    return false;
            }

            if (input.IsNewMouseButtonPress(MouseButtons.RightButton))
            {
                Camera.TrackingBody = _body;
                Camera.ZoomTo = 1 / 3f;
            }

            Vector2 playeraim;
            playeraim.X = position.X - (Weapoon.Body.Position.X);
            playeraim.Y = position.Y - (Weapoon.Body.Position.Y);

            // vecteur support
            Vector2 vecteurbase;
            vecteurbase.X = 1;
            vecteurbase.Y = 0;


            // angle de la visée
            double visee = Math.Atan2(playeraim.Y, playeraim.X);

            // l'arme suit la souris
            Weapoon.Body.Rotation = (float)visee;
            if (visee > Math.PI / 2 || visee < -Math.PI / 2)
            {
                aimsens = false;
            }
            else
            {
                aimsens = true;
            }
            return false;
        }

        public void Walk(bool sens)
        {
            const float torque = 2f;
            int direction;
            if (sens)
                direction = 1;
            else
                direction = -1;
            float force = torque * direction;
            _wheel.ApplyAngularImpulse(force);
        }

        public void Jump(bool sens)
        {
            // empêche de sauter quand on ne touche pas le sol
            if (!IsTouchingGround)
                return;

            int direction;
            if (sens)
                direction = 1;
            else
                direction = -1;

            _wheel.ApplyLinearImpulse(new Vector2(direction * 50, -200));
        }
        #endregion

        public bool SetWeapoon(IWeapoon weapoon)
        {
            if (_weapoon != null)
                if (_weapoon.bullet != null && _weapoon.bullet.Awake)
                    return false;
                else
                    _weapoon.UnAssign();

            _weapoon = weapoon;
            _weapoon.Assign(this);
            return true;
        }
        #region Draw
        public void Draw(ScreenManager sc, GameTime gameTime)
        {
            Vector2 MyPosition = _body.WorldCenter;

            SpriteFont font = sc.Fonts.MenuSpriteFont;

            // affiche le sprite animé ou la position arrêté
            if (IsTouchingGround & !marche.FinishedAnimation)
                marche.Draw(gameTime, 1, sens ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
            else
            {
                sc.SpriteBatch.Draw(_bodySprite.Texture, ConvertUnits.ToDisplayUnits(_body.Position + new Vector2(0, 1)), null,
                    Color.White, _body.Rotation, _bodySprite.Origin, 1f, (sens || aimsens) ? SpriteEffects.None : SpriteEffects.FlipHorizontally, 0f);
            }

            // Affichage de l'arme
            if (_weapoon != null)
                Weapoon.Draw(sc, gameTime, aimsens);

            // affichage de la tête
            sc.SpriteBatch.Draw(_headSprite.Texture, ConvertUnits.ToDisplayUnits(_headBody.Position), null,
                                   Color.White, _headBody.Rotation, _headSprite.Origin, 1f, SpriteEffects.None, 0f);

            // Affichages supplémentaires sur le personnage
            if (drawName)
            {
                sc.SpriteBatch.DrawString(font, Name, NamePositionDrawing(MyPosition, 48f), Equipe.Color, 0f, Vector2.Zero, 3f, SpriteEffects.None, 0f);
            }
            if (drawLife)
            {
                sc.SpriteBatch.DrawString(font, Vie.ToString() + " %", ViePositionDrawing(MyPosition, 32f), DamageColor, 0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);
            }
        }

        private Vector2 NamePositionDrawing(Vector2 myPos, float charsize)
        {
            myPos = ConvertUnits.ToDisplayUnits(myPos);
            myPos = new Vector2(myPos.X - (Name.Length / 2 * charsize), myPos.Y - 6 * 64f);
            return myPos;
        }

        private Vector2 ViePositionDrawing(Vector2 myPos, float charsize)
        {
            myPos = ConvertUnits.ToDisplayUnits(myPos);
            myPos = new Vector2(myPos.X - (Vie.ToString() + " %").Length / 2 * charsize, myPos.Y - 3f * 64f);
            return myPos;
        }
        #endregion

        public RevoluteJoint GetWheelJoint()
        {
            return _wheeljoint;
        }

        #region Overriding System.Object
        public override string ToString()
        {
            return String.Format("", null);
        }
        #endregion

    }

}