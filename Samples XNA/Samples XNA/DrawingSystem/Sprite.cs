﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    public class Sprite
    {
        public Vector2 Origin;
        public Texture2D Texture;
        public Vector2 Position;
        int maxIndex = 0;

        public Sprite(Texture2D texture, Vector2 origin)
        {
            this.Texture = texture;
            this.Origin = origin;
        }

        public Sprite(Texture2D sprite)
        {
            Texture = sprite;
            Origin = new Vector2(sprite.Width / 2f, sprite.Height / 2f);
        }

        public void LoadContent(ScreenManager sc, string assetName, int maxIndex)
        {
            Texture = sc.Content.Load<Texture2D>(assetName);
            this.maxIndex = maxIndex;
        }

        public void Update(GameTime theGameTime, Vector2 theSpeed, Vector2 theDirection)
        {
            Position += theDirection * theSpeed * (float)theGameTime.ElapsedGameTime.TotalSeconds;
        }

    }
}