﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarseerPhysics.SamplesFramework
{
    public class GameSettings
    {
        public bool Pause {get; set;}
        public bool SoundEnabled { get; set; }
        public bool EffectsEnabled { get; set; }

        private float worldStep;
        public float WorldStep 
        {
            get
            {
                return worldStep;
            }

            set
            {
                if (!Pause)
                    worldStep = Math.Min(value, 1f / 30f);
                else
                    worldStep = 0;
            }
        }

        public GameSettings()
        {
            Pause = false;
        }
    }

}
