﻿using System;
using System.Collections.Generic;
using FarseerPhysics.DebugViews;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace FarseerPhysics.SamplesFramework
{
    public class MultiGameScreen : GameScreen
    {
        public Camera2D Camera;
        protected DebugViewXNA DebugView;
        protected World World;
        protected Map myMap;
        protected List<Team> equipes = new List<Team>();
        protected List<GameScreen> screens = new List<GameScreen>();

        // particles
        protected ParticleEngine particleEngine;

        const float _maxSpeed = 50f;


        private float _agentForce;
        private float _agentTorque;
        private Body _userAgent;

        protected MultiGameScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(0.75);
            TransitionOffTime = TimeSpan.FromSeconds(0.75);
            HasCursor = true;
            EnableCameraControl = true;
            _userAgent = null;
            World = null;
            Camera = null;
            DebugView = null;
        }

        public bool EnableCameraControl { get; set; }

        protected void SetUserAgent(Body agent, float force, float torque)
        {
            _userAgent = agent;
            _agentForce = force;
            _agentTorque = torque;
        }

        public override void LoadContent()
        {
            base.LoadContent();
            ScreenManager.SoundEngine.StopMusic();

            // Load the particle engine
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/circle"));
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/star"));
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/diamond"));
            particleEngine = new ParticleEngine(textures, new Vector2(400, 240));

            //We enable diagnostics to show get values for our performance counters.
            Settings.EnableDiagnostics = true;

            if (World == null)
            {
                World = new World(Vector2.Zero);
            }
            else
            {
                World.Clear();
            }

            if (DebugView == null)
            {
                DebugView = new DebugViewXNA(World);
                DebugView.RemoveFlags(DebugViewFlags.Shape);
                DebugView.RemoveFlags(DebugViewFlags.Joint);
                DebugView.DefaultShapeColor = Color.White;
                DebugView.SleepingShapeColor = Color.LightGray;
                DebugView.LoadContent(ScreenManager.GraphicsDevice, ScreenManager.Content);
            }

            if (Camera == null)
            {
                Camera = new Camera2D(ScreenManager.GraphicsDevice);
            }
            else
            {
                Camera.ResetCamera();
            }
            ScreenManager.GraphicsDevice.Clear(Color.DeepSkyBlue);
            // Loading may take a while... so prevent the game from "catching up" once we finished loading
            ScreenManager.Game.ResetElapsedTime();

            // Background avec scrolling
            ScrollableBackgroundScreen scrollbg = new ScrollableBackgroundScreen(ScreenManager);
            screens.Add(scrollbg);
            ScreenManager.AddScreen(scrollbg);

            World.Gravity = new Vector2(0f, 9.81f);

            myMap = new Map(World, ScreenManager, Camera, 12);

            // Zoom par défaut
            Camera.Zoom = 0.3f;

            // lance la musique
            ScreenManager.SoundEngine.PlayMusic(SoundAmbiance.campagne1);

        }

        public override void UnloadContent()
        {
            foreach (GameScreen gs in screens)
            {
                gs.ExitScreen();
            }
            base.UnloadContent();
        }
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            ScreenManager.GameSettings.WorldStep = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!coveredByOtherScreen && !otherScreenHasFocus)
            {
                // variable time step but never less then 30 Hz
                World.Step(ScreenManager.GameSettings.WorldStep);
            }
            else
            {
                World.Step(0f);
            }
            Camera.Update(gameTime);
            particleEngine.Update(gameTime);
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        public override void HandleInput(InputHelper input, GameTime gameTime)
        {
            // Control debug view
            if (input.IsNewKeyPress(Keys.Escape))
            {
                if (!isMaster)
                    ExitScreen();
                else
                    ScreenManager.Game.Exit();
            }

            if (_userAgent != null)
            {
                HandleUserAgent(input);
            }

            if (EnableCameraControl)
            {
                HandleCamera(input, gameTime);
            }

            if (input.IsNewKeyPress(Keys.P))
                ScreenManager.GameSettings.Pause = !ScreenManager.GameSettings.Pause;

            if (equipes.Count < 1)
            {
                return;
            }

            Character ActiveCharacter = TeamHandler.GetCurrentCharacter(equipes);
            //Camera.TrackingBody = ActiveCharacter.Body;
            if (ActiveCharacter.Body.BodyType != BodyType.Dynamic)
                ActiveCharacter.Body.BodyType = BodyType.Dynamic;

            ActiveCharacter.Handle(input, gameTime);

            if (Math.Abs(ActiveCharacter.GetWheelJoint().MotorSpeed) < _maxSpeed * 0.06f)
            {
                ActiveCharacter.GetWheelJoint().MotorEnabled = false;
            }
            else
            {
                ActiveCharacter.GetWheelJoint().MotorEnabled = true;
            }


            if (input.IsNewKeyPress(Keys.Tab))
            {
                TeamHandler.SetNextCharacter(ref equipes); // TODO => gestion des personnages
            }

            if (input.IsNewKeyPress(Keys.K))
                myMap.DestroyMSArea(10f, Camera.ConvertScreenToWorld(input.Cursor));

            if (HasCursor)
                HandleCursor(input, ActiveCharacter);

            base.HandleInput(input, gameTime);
        }

        private void HandleCursor(InputHelper input, Character ActiveChar)
        {
            ActiveChar.HandleMouse(input, Camera);
        }

        private void HandleCamera(InputHelper input, GameTime gameTime)
        {
            Vector2 camMove = Vector2.Zero;

            if (input.KeyboardState.IsKeyDown(Keys.Up))
            {
                camMove.Y -= 15f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if (input.KeyboardState.IsKeyDown(Keys.Down))
            {
                camMove.Y += 15f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if (input.KeyboardState.IsKeyDown(Keys.Left))
            {
                camMove.X -= 15f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if (input.KeyboardState.IsKeyDown(Keys.Right))
            {
                camMove.X += 15f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            if (input.KeyboardState.IsKeyDown(Keys.PageUp))
            {
                Camera.Zoom += 5f * (float)gameTime.ElapsedGameTime.TotalSeconds * Camera.Zoom / 20f;
            }
            if (input.KeyboardState.IsKeyDown(Keys.PageDown))
            {
                Camera.Zoom -= 5f * (float)gameTime.ElapsedGameTime.TotalSeconds * Camera.Zoom / 20f;
            }
            if (camMove != Vector2.Zero)
            {
                Camera.MoveCamera(camMove);
            }
            if (input.IsNewKeyPress(Keys.Home))
            {
                Camera.ResetCamera();
            }
        }

        private void HandleUserAgent(InputHelper input)
        {
            Vector2 force = _agentForce * new Vector2(input.GamePadState.ThumbSticks.Right.X,
                                                      -input.GamePadState.ThumbSticks.Right.Y);
            float torque = _agentTorque * (input.GamePadState.Triggers.Right - input.GamePadState.Triggers.Left);

            _userAgent.ApplyForce(force);
            _userAgent.ApplyTorque(torque);

            float forceAmount = _agentForce * 0.6f;

            force = Vector2.Zero;
            torque = 0;

            if (input.KeyboardState.IsKeyDown(Keys.A))
            {
                force += new Vector2(-forceAmount, 0);
            }
            if (input.KeyboardState.IsKeyDown(Keys.S))
            {
                force += new Vector2(0, forceAmount);
            }
            if (input.KeyboardState.IsKeyDown(Keys.D))
            {
                force += new Vector2(forceAmount, 0);
            }
            if (input.KeyboardState.IsKeyDown(Keys.W))
            {
                force += new Vector2(0, -forceAmount);
            }
            if (input.KeyboardState.IsKeyDown(Keys.Q))
            {
                torque -= _agentTorque;
            }
            if (input.KeyboardState.IsKeyDown(Keys.E))
            {
                torque += _agentTorque;
            }

            _userAgent.ApplyForce(force);
            _userAgent.ApplyTorque(torque);
        }

        private void EnableOrDisableFlag(DebugViewFlags flag)
        {
            if ((DebugView.Flags & flag) == flag)
            {
                DebugView.RemoveFlags(flag);
            }
            else
            {
                DebugView.AppendFlags(flag);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            Matrix projection = Camera.SimProjection;
            Matrix view = Camera.SimView;

            DebugView.RenderDebugData(ref projection, ref view);

            //ScreenManager.SpriteBatch.Begin();
            //ScreenManager.SpriteBatch.DrawString(ScreenManager.Fonts.DetailsFont, Camera.Zoom.ToString(), Vector2.Zero, Color.White);
            //ScreenManager.SpriteBatch.End();

            // Affichage de la carte
            myMap.Draw(ScreenManager, Camera);

            // Particules
            particleEngine.Draw(ScreenManager.SpriteBatch);

            //ScreenManager.SpriteBatch.Begin(0, null, null, null, null, null, Camera.View);

            ////fin du spritebatch
            //ScreenManager.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}