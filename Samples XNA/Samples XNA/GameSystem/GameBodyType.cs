﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FarseerPhysics.SamplesFramework
{
    public enum GameBodyType
    {
        Map,
        BodyCharacter,
        WheelCharacter,
        Weapoon,
    }
}
