﻿using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FarseerPhysics.SamplesFramework
{
    internal class Bateaux : PhysicsGameScreen, IDemoScreen
    {
        #region IDemoScreen Members

        public string GetTitle()
        {
            return "Bateaux";
        }

        public string GetDetails()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Bienvenue dans Modern Invaders !");
            sb.AppendLine(string.Empty);
            sb.AppendLine("  - Deplacez-vous de gauche a droite en utilisant les touches Q et D");
            sb.AppendLine("  - Sautez avec la touche espace");
            sb.AppendLine("  - Visez grace a la souris et cliquez pour tirer !");
            return sb.ToString();
        }

        public override GameScreen Reset()
        {
            return new Bateaux();
        }

        #endregion

        Map myMap;
        List<Team> equipes = new List<Team>();
        Team epiteens;
        Team profs;
        List<GameScreen> screens = new List<GameScreen>();

        Character Rambo;
        Character enemy;

        // particles
        ParticleEngine particleEngine;

        private float _timer = 60;

        const float _maxSpeed = 100f;

        public List<Vector2> positions = new List<Vector2>();

        #region (Un)Load Content
        public override void LoadContent()
        {
            base.LoadContent();

            positions.Add(new Vector2(-226f, -98f));
            positions.Add(new Vector2(-57f, 64f));
            positions.Add(new Vector2(-42f, -9f));
            positions.Add(new Vector2(32, -111));
            positions.Add(new Vector2(184, -5));
            Random rnd = new Random();
            positions = positions.OrderBy(item => rnd.Next()).ToList();
            ScrollableBackgroundScreen scrollbg = new ScrollableBackgroundScreen(ScreenManager);
            screens.Add(scrollbg);
            ScreenManager.AddScreen(scrollbg);

            World.Gravity = new Vector2(0f, 10f);

            myMap = new Map(World, ScreenManager, Camera, 12);
            myMap.MSTerrainFromTexture(ScreenManager.Content.Load<Texture2D>("Maps/Bateaux"));
            epiteens = new Team("Epiteens", World, ScreenManager)
            {
                thisGameScreen = this,
                levelMap = myMap,
                Teams = equipes
            };
            profs = new Team("Profs", World, ScreenManager)
            {
                levelMap = myMap,
                thisGameScreen = this,
                Teams = equipes
            };
            enemy = new Character(World, positions[0], "Krisboul", ScreenManager);
            Rambo = new Character(World, positions[1], "GolluM", ScreenManager);
            epiteens.LoadCharacter(Rambo);
            epiteens.LoadCharacter(enemy);

            Character Jules = new Character(World, positions[2], "Jules", ScreenManager);
            Character Bertrand = new Character(World, positions[3], "Bertrand", ScreenManager);
            profs.LoadCharacter(Jules);
            profs.LoadCharacter(Bertrand);
            //  Camera.TrackingBody = epiteens.GetActiveChar().Body;
            equipes.Add(epiteens);
            equipes.Add(profs);

            TeamHandler.SetNextCharacter(ref equipes);

            Camera.Zoom = 1 / 7f;
            Camera.DefaultZoom = 1 / 3f;
            Camera.TrackingBody = TeamHandler.GetCurrentCharacter(equipes).Body;

            // Load the particle engine
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/circle"));
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/star"));
            textures.Add(ScreenManager.Content.Load<Texture2D>("particles/diamond"));
            particleEngine = new ParticleEngine(textures, new Vector2(400, 240));
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            foreach (GameScreen gs in screens)
            {
                gs.ExitScreen();
            }
            foreach (Body b in World.BodyList)
            {
                World.RemoveBody(b);
            }
            World = new World(new Vector2(0, 9.81f));
        }
        #endregion

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            // mise à jour des particules
            particleEngine.Update(gameTime);

            foreach (Team t in equipes)
            {
                t.Update(gameTime, Camera);
            }

            _timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (_timer <= 0f)
            {
                _timer = 60;
                TeamHandler.SetNextCharacter(ref equipes);
                Camera.TrackingBody = TeamHandler.GetCurrentCharacter(equipes).Body;
            }
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

        }
        #region input
        public override void HandleInput(InputHelper input, GameTime gameTime)
        {

            Character ActiveCharacter = TeamHandler.GetCurrentCharacter(equipes);
            if (ActiveCharacter.Body.BodyType != BodyType.Dynamic)
                ActiveCharacter.Body.BodyType = BodyType.Dynamic;

            ActiveCharacter.Handle(input, gameTime);

            if (Math.Abs(ActiveCharacter.GetWheelJoint().MotorSpeed) < _maxSpeed * 0.06f)
            {
                ActiveCharacter.GetWheelJoint().MotorEnabled = false;
            }
            else
            {
                ActiveCharacter.GetWheelJoint().MotorEnabled = true;
            }


            //if (input.IsNewKeyPress(Keys.Tab)) // FIXME
            //{
            //    TeamHandler.SetNextCharacter(ref equipes);
            //}

            //if (input.IsNewKeyPress(Keys.K))
            //    myMap.DestroyMSArea(10f, Camera.ConvertScreenToWorld(input.Cursor));

            if (ActiveCharacter.hasPlayed)
            {
                //TeamHandler.SetNextCharacter(ref equipes);
                //Camera.TrackingBody = TeamHandler.GetCurrentCharacter(equipes).Body;
                _timer = 60;

            }

            if (HasCursor)
                HandleCursor(input, ActiveCharacter);

            base.HandleInput(input, gameTime);
        }

        private void HandleCursor(InputHelper input, Character ActiveChar)
        {
            if (ActiveChar.HandleMouse(input, Camera))
            {
            }
        }
        #endregion
        #region Draw
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.SpriteBatch.Begin();
            ScreenManager.SpriteBatch.DrawString(ScreenManager.Fonts.DetailsFont, _timer.ToString(), Vector2.Zero, Color.White);
            ScreenManager.SpriteBatch.End();

            // Affichage de la carte
            myMap.Draw(ScreenManager, Camera);

            // Particules
            particleEngine.Draw(ScreenManager.SpriteBatch);

            ScreenManager.SpriteBatch.Begin(0, null, null, null, null, null, Camera.View);

            // Affichage des équipes
            foreach (Team t in equipes)
            {
                t.DrawAll(ScreenManager, gameTime);
            }

            //fin du spritebatch
            ScreenManager.SpriteBatch.End();
            base.Draw(gameTime);
        }
        #endregion
    }
}